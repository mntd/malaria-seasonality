# Malaria seasonality

This repository contains sample data and code for the analysis in Nguyen et al. (2020), 'Mapping malaria seasonality in Madagascar using health facility data':

- sample_data_1.csv: RDT positive counts (rdt_pos) from four anonymised health facilities with given ids (hf.id). 
- sample_data_2.csv: Empirical monthly proportions (p.bar) and one realisation from the fitted model ("1"/"X1") for another four anonymised health facilities as well as the median, lower and upper bounds of the monthly proportions as calculated from 1000 realisations ("p.bar.adj", "p.bar.adj.lower" and "p.bar.adj.upper").
- script_to_share.R: R script to illustrate the monthly proportion calculations and seasonal feature extraction. Figures 1 and S5 in the paper are recreated using sample_data_1.csv while Figures 6 and S3 are recreated using sample_data_2.csv.
- model_fitting.R: A sample R script to show key model analysis steps and the functions used to conduct model fitting and evaluation. Note that the full dataset is required to run this script (see 'Availability of data and material' statement in paper).
- utility_fns_to_share.R: R script containing some key functions used for model fitting and evaluation.

Please do let me know if you encounter any errors.
